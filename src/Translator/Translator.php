<?php declare(strict_types=1);
/** *****************************************************************************************************************
 *  Translator.php
 *  *****************************************************************************************************************
 *  @copyright 2022 Flagstone
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 *  *****************************************************************************************************************
 *  Created: 2022/01/25
 *  ***************************************************************************************************************** */
namespace Flagstone\TranslatorBundle\Translator;

use Flagstone\TranslatorBundle\Translator\Exception\EmptyTemplateException;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Contracts\Translation\TranslatorInterface;

class Translator
{
    private TranslatorInterface $translator;

    private RequestStack $request;

    private string $message;

    private string $template;

    private ?string $locale;

    private ?array $options;

    private ?string $domain;

    /**
     *  Translator constructor.
     *
     *  @param TranslatorInterface $translator
     *  @param RequestStack $request
     */
    public function __construct(TranslatorInterface $translator, RequestStack $request)
    {
        $this->translator = $translator;
        $this->request = $request;
    }

    /**
     *  Populate the Translator object
     *
     *  @param  string          $template
     *  @param  array|null      $options
     *  @param  string|null     $domain
     *  @param  string|null     $locale
     *  @return $this
     */
    public function trans(string $template, ?array $options = [], ?string $domain = null, ?string $locale = null): self
    {
        $this->template = $template;
        $this->options = $options;
        $this->domain = $domain;
        $this->locale = $locale;

        return $this;
    }

    /**
     *  Get the translated message. Throw an exception if the id or the string to translate is empty
     *
     *  @return string
     *  @throws EmptyTemplateException
     */
    public function getMessage(): string
    {
        if (empty($this->template)) {
            throw new EmptyTemplateException('string or id to translate cannot be empty.');
        }

        return $this->translator->trans(
            $this->template,
            $this->options,
            $this->domain,
            $this->locale
        );
    }

    /**
     *  Change the locale to translate the string or the id in another locale without to define all attributes
     *  Usage :
     *      $messageInLocale = $this->translator->trans($string)->getMessage()
     *      $messageInFrench = $messageInLocale->changeLocale('fr')->getMessage()
     *
     *  @param  string|null $locale
     *  @return $this
     */
    public function changeLocale(?string $locale = null): self
    {
        if (empty($locale)) {
            if (null !== $this->request->getCurrentRequest()) {
                if (null !== $this->request->getCurrentRequest()->getLocale()) {
                    $locale = $this->request->getCurrentRequest()->getLocale();
                } else {
                    $locale = $this->request->getCurrentRequest()->getDefaultLocale();
                }
            } else {
                $locale = 'en';
            }
        }
        $this->locale = $locale;

        return $this;
    }
}